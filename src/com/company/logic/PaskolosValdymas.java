package com.company.logic;

public class PaskolosValdymas {

    private static Paskola[] paskoluTalpykla = new Paskola[100];

    public static String atvaizduokPaskolosLentele(Paskola paskola) {
        MenesioInformacija[] lentele = paskola.getMenesioInformacija();

        StringBuilder sb = new StringBuilder();
        //header
        sb.append("Men Nr. | ");
        sb.append("Grazintina dalis | ");
        sb.append("Likutis Eur. | ");
        sb.append("Priskaiciuotos palukanos Eur. | ");
        sb.append("Bendra moketina suma Eur. |\n");
        for (int i = 0; i < lentele.length; i++) {
            sb.append(String.format("%-8d|", (i + 1)));
            sb.append(String.format("%-18s|", lentele[i].getGrazintinaDalis()));
            sb.append(String.format("%-14s|", lentele[i].getLikutis()));
            sb.append(String.format("%-31s|", lentele[i].getPriskaiciuotosPalukanos()));
            sb.append(String.format("%-27s|", lentele[i].getBendraMoketinaSuma()));
            sb.append("\n");
        }
        sb.append("Pagal si plana reikes sumoje sumoketi " + paskola.getImokuSumaPerVisaLaikotarpi());
        return sb.toString();
    }

    public static String grazinkLentelePagalId(int id) {
        Paskola paskola = getPaskolaById(id);
        if (paskola != null) {
                return atvaizduokPaskolosLentele(paskola);
        }
        return "";
    }

    public static String suformuotiPaskolaIrPatalpinti(double suma, double palukasProc, int menesiai, double sutartiesMokestis) throws Exception {
        Paskola paskola = new Paskola(suma, palukasProc, menesiai, sutartiesMokestis);
        patalpintiPaskolaITalpykla(paskola);
        String lentele = atvaizduokPaskolosLentele(paskola);
        return lentele;
    }

    public static String suformuotiPaskolaIrParodytiLentele(double suma, double palukasProc, int menesiai, double sutartiesMokestis) throws Exception {
        Paskola paskola = new Paskola(suma, palukasProc, menesiai, sutartiesMokestis);
        String lentele = atvaizduokPaskolosLentele(paskola);
        return lentele;
    }

    public static Paskola suformuotiPaskola(double suma, double palukasProc, int menesiai, double sutartiesMokestis) {
        return new Paskola(suma, palukasProc, menesiai, sutartiesMokestis);
    }

    public static int patalpintiPaskolaITalpykla(Paskola paskola) throws Exception {
        int space = findFirstFreeSpaceStorage();
        paskoluTalpykla[space] = new Paskola();
        paskoluTalpykla[space] = paskola;
        System.out.println("Paskola id=" + space + " ideta");
        return space;
    }

    public static int findFirstFreeSpaceStorage() throws NullPointerException {
        for (int i = 0; i < paskoluTalpykla.length; i++) {
            if (paskoluTalpykla[i] == null) {
                return i;
            }
        }
        throw new NullPointerException("Error: Nera talpykloje vietos!");
    }

    public static Paskola getPaskolaById(int id) {
        if (paskoluTalpykla[id] != null) {
            return paskoluTalpykla[id];
        }
        return null;
    }

    public static void displayPaskolaById(int id) {
        if (paskoluTalpykla[id] != null) {
            atvaizduokPaskolosLentele(paskoluTalpykla[id]);
        }
    }

    public static String bendraPaskolosInformacijaById(int id) {
        String st = "id:" + id + " " + getPaskolaById(id).toString();
        return st;
    }

    public static void spausdinkVisasPaskolas() {
        for (int i = 0; i < paskoluTalpykla.length; i++) {
            try {

                System.out.println(bendraPaskolosInformacijaById(i));
            } catch (NullPointerException ex) {

            }
        }
    }
}
