package com.company.logic;

public class PalukanuSkaiciavimoPagalbininkas {

    public static MenesioInformacija[] sudarytiLentele(Paskola paskola) {
        MenesioInformacija[] menesiuField = new MenesioInformacija[paskola.getLaikotarpisMen()];

        double visaSuma = paskola.getSuma() + paskola.getSutartiesMokestis();
        double grazintinaMenesineDalis = visaSuma / paskola.getLaikotarpisMen();
        double menesioPalukana = paskola.getPalukanosProc() / paskola.getLaikotarpisMen() / 100;

        double totalSum = 0.0;
        //initial
        menesiuField[0] = new MenesioInformacija();
        menesiuField[0].setGrazintinaDalis(grazintinaMenesineDalis);
        menesiuField[0].setLikutis(visaSuma - grazintinaMenesineDalis);
        menesiuField[0].setPriskaiciuotosPalukanos(menesiuField[0].getLikutis() * menesioPalukana);
        menesiuField[0].setBendraMoketinaSuma(menesiuField[0].getGrazintinaDalis() + menesiuField[0].getPriskaiciuotosPalukanos());

        totalSum += menesiuField[0].getBendraMoketinaSuma();

        for (int i = 1; i < menesiuField.length; i++) {
            menesiuField[i] = new MenesioInformacija();
            menesiuField[i].setGrazintinaDalis(grazintinaMenesineDalis);
            menesiuField[i].setLikutis(menesiuField[i - 1].getLikutis() - grazintinaMenesineDalis);
            menesiuField[i].setPriskaiciuotosPalukanos(menesiuField[i].getLikutis() * menesioPalukana);
            menesiuField[i].setBendraMoketinaSuma(menesiuField[i].getGrazintinaDalis() + menesiuField[i].getPriskaiciuotosPalukanos());
            totalSum += menesiuField[i].getBendraMoketinaSuma();
        }
        paskola.setImokuSumaPerVisaLaikotarpi(totalSum);
        paskola.setTableCalculated(true);
        return menesiuField;
    }

    public static double totalSumOverTime(Paskola paskola) {
        return paskola.getImokuSumaPerVisaLaikotarpi();
    }
}
