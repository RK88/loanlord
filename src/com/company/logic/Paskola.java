package com.company.logic;

import java.util.Arrays;

public class Paskola {

    private double suma;
    private double palukanosProc;
    private int laikotarpisMen;
    private double sutartiesMokestis;
    private MenesioInformacija[] menesioInformacija;

    //private boolean tableCalculated;

    private double imokuSumaPerVisaLaikotarpi = 0.0;

    public Paskola(double suma, double palukanosProc, int laikotarpisMen, double sutartiesMokestis) {
        this.suma = suma;
        this.palukanosProc = palukanosProc;
        this.laikotarpisMen = laikotarpisMen;
        this.sutartiesMokestis = sutartiesMokestis;
        menesioInformacija = new MenesioInformacija[laikotarpisMen];
        menesioInformacija = PalukanuSkaiciavimoPagalbininkas.sudarytiLentele(this);
       // this.tableCalculated = false;
    }

    public Paskola() {
    }

    public MenesioInformacija[] getMenesioInformacija() {
        return menesioInformacija;
    }

    public void setMenesioInformacija(MenesioInformacija[] menesioInformacija) {
        this.menesioInformacija = menesioInformacija;
    }

    public double getSuma() {
        return suma;
    }

    public void setSuma(double suma) {
        this.suma = suma;
    }

    public double getPalukanosProc() {
        return palukanosProc;
    }

    public void setPalukanosProc(double palukanosProc) {
        this.palukanosProc = palukanosProc;
    }

    public int getLaikotarpisMen() {
        return laikotarpisMen;
    }

    public void setLaikotarpisMen(int laikotarpisMen) {
        this.laikotarpisMen = laikotarpisMen;
    }

    public double getSutartiesMokestis() {
        return sutartiesMokestis;
    }

    public void setSutartiesMokestis(double sutartiesMokestis) {
        this.sutartiesMokestis = sutartiesMokestis;
    }

    public double getImokuSumaPerVisaLaikotarpi() {
        return imokuSumaPerVisaLaikotarpi;
    }

    public void setImokuSumaPerVisaLaikotarpi(double imokuSumaPerVisaLaikotarpi) {
        this.imokuSumaPerVisaLaikotarpi = imokuSumaPerVisaLaikotarpi;
    }

    //public boolean isTableCalculated() {
   //     return tableCalculated;
   // }

    public void setTableCalculated(boolean tableCalculated) {
       // this.tableCalculated = tableCalculated;
    }

    @Override
    public String toString() {
        return "Paskola{" +
                "suma=" + suma +
                ", palukanosProc=" + palukanosProc +
                ", laikotarpisMen=" + laikotarpisMen +
                ", sutartiesMokestis=" + sutartiesMokestis +
                ", imokuSumaPerVisaLaikotarpi=" + imokuSumaPerVisaLaikotarpi +
                '}';
    }
}
