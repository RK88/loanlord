package com.company.logic;

public class MenesioInformacija {
    private double grazintinaDalis;
    private double likutis;
    private double priskaiciuotosPalukanos;
    private double bendraMoketinaSuma;

    public MenesioInformacija() {
    }

    public MenesioInformacija(double grazintinaDalis, double likutis, double priskaiciuotosPalukanos, double bendraMoketinaSuma) {
        this.grazintinaDalis = grazintinaDalis;
        this.likutis = likutis;
        this.priskaiciuotosPalukanos = priskaiciuotosPalukanos;
        this.bendraMoketinaSuma = bendraMoketinaSuma;
    }

    public double getGrazintinaDalis() {
        return grazintinaDalis;
    }

    public void setGrazintinaDalis(double grazintinaDalis) {
        this.grazintinaDalis = grazintinaDalis;
    }

    public double getLikutis() {
        return likutis;
    }

    public void setLikutis(double likutis) {
        this.likutis = likutis;
    }

    public double getPriskaiciuotosPalukanos() {
        return priskaiciuotosPalukanos;
    }

    public void setPriskaiciuotosPalukanos(double priskaiciuotosPalukanos) {
        this.priskaiciuotosPalukanos = priskaiciuotosPalukanos;
    }

    public double getBendraMoketinaSuma() {
        return bendraMoketinaSuma;
    }

    public void setBendraMoketinaSuma(double bendraMoketinaSuma) {
        this.bendraMoketinaSuma = bendraMoketinaSuma;
    }
}
