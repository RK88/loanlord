package com.company.Console;

import com.company.logic.PaskolosValdymas;

public class PaskolaMenu {

    public static void entryMenu() {
        clearScreen();
        System.out.println("=== Ivadinis meniu ===");
        System.out.println("A: Naujas skaiciavimas");
        System.out.println("B: Perziureti ankstesnius skaiciavimus");
        System.out.println("C: - Greitas skaičiavimas");
        System.out.println("E: Baigti darba");

        char choice = UserInput.enterChar("Pasirinkite varianta", "ABCE");

        switch (choice) {
            case 'a':
                // System.out.println("Naujas skaiciavimas");
                naujasSkaiciavimasMenu(true);
                break;
            case 'b':
                perziuretiAnkstesniusSkaiciavimus();
                break;
            case 'c':
                naujasSkaiciavimasMenu(false);
                break;
            case 'e':
                System.out.println("Iseiname");
                System.exit(0);
            default:
                System.out.println("Neaiskus pasirinkimas");
        }
    }

    private static void greitasSkaiciavimasMeniu() {
        System.out.println("=== Greitas skaiciavimas ===");

    }

    //B
    private static void perziuretiAnkstesniusSkaiciavimus() {
        System.out.println("=== Visi Irasai ===");
        PaskolosValdymas.spausdinkVisasPaskolas();
        israsuMeniu();
    }

    //inside B
    public static void israsuMeniu() {
        clearScreen();
        System.out.println("=== Paskolu duomenys ===");
        System.out.println("A: Detaliau perziureti kuri nors skaiciavima");
        System.out.println("B: Grizti i pagrindini meniu");
        char choice = UserInput.enterChar("", "AB");
        switch (choice) {
            case 'a':
                detaliosPaskolosPasirinkimoMenu();
                break;
            case 'b':
                entryMenu();
                return;
        }
    }

    private static void detaliosPaskolosPasirinkimoMenu() {
        int id = UserInput.enterInt("Pasirinkite jus dominanti paskolos id:");
        String lenteleString = PaskolosValdymas.grazinkLentelePagalId(id);
        System.out.println(lenteleString.equals("") ? "Paskolos su id " + id + " nera." : lenteleString);
        israsuMeniu();
    }

    //A
    public static void naujasSkaiciavimasMenu(boolean shouldIStore) {
        clearScreen();
        System.out.println(shouldIStore?"=== Naujas paskolos skaiciavimas ===":"=== Greitas paskolos skaiciavimas");
        double sum = UserInput.enterDouble("Iveskite suma: ");
        double proc = UserInput.enterDouble("Iveskite palukanas (proc): ");
        int men = UserInput.enterInt("Iveskite laikotarpi (men): ");
        double mokestis = UserInput.enterDouble("Iveskite sutarties mokesti: ");

        try {
            //PaskolosValdymas.suformuotiPaskola(sum, proc, men, mokestis);
            String resultLentele;
            if (shouldIStore){
                resultLentele = PaskolosValdymas.suformuotiPaskolaIrPatalpinti(sum, proc, men, mokestis);
            }else{
                resultLentele = PaskolosValdymas.suformuotiPaskolaIrParodytiLentele(sum, proc, men, mokestis);
            }
            System.out.println(resultLentele);

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            System.exit(0);
        }

        System.out.println("A: Kartoti operacija");
        System.out.println("B: Grizti i pagrindini meniu");

        char choice = UserInput.enterChar("","AB");
        switch (choice)
        {
            case 'a':
                naujasSkaiciavimasMenu(shouldIStore);
                break;
            case 'b':
                entryMenu();
                return;
        }
    }

    public static void clearScreen() {
        System.out.print("\033[H\033[2J");
        System.out.flush();
    }
}
