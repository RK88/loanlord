package com.company.Console;

import java.util.Scanner;

public class UserInput {
    private static Scanner sc = new Scanner(System.in);

    public static double enterDouble(String message) {
        double result = 0;

        do {
            System.out.println(message);
            String input = sc.next();
            try {
                result = Double.parseDouble(input);
                System.out.println(result);
                return result;
            } catch (Exception ex) {
                System.out.println("you must enter numeric value");
            }
        } while (true);
    }

    public static int enterInt(String message) {
        int result = 0;

        do {
            System.out.println(message);
            String input = sc.next();
            try {
                result = Integer.parseInt(input);
                System.out.println(result);
                return result;
            } catch (Exception ex) {
                System.out.println("you must enter int value");
            }
        } while (true);
    }

    public static char enterChar(String message, String validChoices) {
        do {
            System.out.println(message);
            String input = sc.next();
            if (input.length() == 1) {
                for (int i = 0; i < validChoices.length() ; i++) {
                    if (input.toLowerCase().charAt(0) ==Character.toLowerCase(validChoices.charAt(i))) {
                        return Character.toLowerCase(input.charAt(0));
                    }
                }
            }
        } while (true);
    }
}
