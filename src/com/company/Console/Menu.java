package com.company.Console;

import java.util.Scanner;

public class Menu {
    Menu[] menus;
    int numberOfSubmenus;
    Scanner sc = new Scanner(System.in);

    public Menu(int numberOfSubmenus) {
        this.numberOfSubmenus = numberOfSubmenus;
        menus = new Menu[numberOfSubmenus];
    }
}
