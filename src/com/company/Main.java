package com.company;

import com.company.Console.PaskolaMenu;
import com.company.Console.UserInput;
import com.company.logic.PalukanuSkaiciavimoPagalbininkas;
import com.company.logic.Paskola;
import com.company.logic.PaskolosValdymas;

public class Main {

    public static void main(String[] args) {
	// write your code here

        double suma = 900;
        double palukanosProc = 10;
        int laikotarpisMen = 10;
        double sutartiesMokestis = 100;

        Paskola paskola = new Paskola(suma, palukanosProc, laikotarpisMen, sutartiesMokestis);
        paskola.setMenesioInformacija(PalukanuSkaiciavimoPagalbininkas.sudarytiLentele(paskola));
        PaskolosValdymas.atvaizduokPaskolosLentele(paskola);

        PaskolaMenu.entryMenu();
    }
}
