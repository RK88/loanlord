import com.company.logic.PalukanuSkaiciavimoPagalbininkas;
import com.company.logic.Paskola;
import com.company.logic.PaskolosValdymas;
import org.junit.Assert;
import org.junit.Test;

public class TestPaskolos {


    @Test
    public void paskolaTotal_totalSum() {
        Paskola paskola = new Paskola(900, 10, 10, 100);
        PalukanuSkaiciavimoPagalbininkas.sudarytiLentele(paskola);

        double actual = PalukanuSkaiciavimoPagalbininkas.totalSumOverTime(paskola);
        double expected = 1045;

        Assert.assertEquals((int) actual, (int) expected);
    }

    @Test
    public void freeSpaceFoundInPaskoluTalpykla() {
        int actual = PaskolosValdymas.findFirstFreeSpaceStorage();
        int expected = 0;
        Assert.assertEquals(actual, expected);
    }

    @Test
    public void testPaskolaExistsInTalpykla() throws Exception {
        int initialSuma = 100;
        Paskola paskola = new Paskola(initialSuma,100,24,100);
        PaskolosValdymas.patalpintiPaskolaITalpykla(paskola);
        Paskola gautaPaskola = PaskolosValdymas.getPaskolaById(0);
        Assert.assertEquals(initialSuma, gautaPaskola.getSuma(),0.0001);
    }
}
